# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    
    mot, l = "", "S"
    while( n > 0):
        mot = mot + l
        n = n - 1
    mot = mot + "0"
    return mot

def S(n: str) -> str:
    l = "S"
    l += n
    return l


def addition(a: str, b: str) -> str:
    if a == "0": return b
    if a.startswith("S"):
        return S(addition(a[1:], b))

def multiplication(a: str, b: str) -> str:
    n, l, mot = int(len(a) - 1) * int(len(b) - 1), "S", ""
    while(n > 0):
        mot = mot + l 
        n = n - 1
    mot = mot + "0"
    return mot

def facto_ite(n: int) -> int:
    nb = 1
    while(n > 0):
        nb = nb * n
        n = n - 1
    return nb


def facto_rec(n: int) -> int:
    nb = 1
    while(n > 0):
        nb = nb * n
        n = n -1
    return nb


def fibo_rec(n: int) -> int:
    if(n == 0):
        return 0
    elif(n == 1):
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    if(n == 0):
        return 0
    elif(n == 1):
        return 1
    else:
        return fibo_ite(n-1) + fibo_ite(n-2)


def golden_phi(n: int) -> int:
    return 17 - n 


def sqrt5(n: int) -> int:
    return 17 - n

def pow(a: float, n: int) -> float:
    nb = a
    while((n-1) > 0):
        a = a * nb
        n = n - 1
    return a
